package com.strekha.quiz

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*


enum class Difficulty {

    ANY, EASY, MEDIUM, HARD
}

data class Result(
        @SerializedName("response_code")
        val responseCode: Int,
        @SerializedName("results")
        val questions: List<Question>
)


data class Question(
        @SerializedName("question")
        val questionText: String,
        @SerializedName("correct_answer")
        val correctAnswer: String,
        @SerializedName("incorrect_answers")
        val incorrectAnswers: List<String>,
        var userAnswer: String?
) : Serializable {
    fun answers(): List<String> {
        val list = ArrayList<String>()
        list.addAll(incorrectAnswers)
        list.add(correctAnswer)
        Collections.shuffle(list)
        return list
    }
}

data class Row(
        val person: String = "",
        val result: Int = 0,
        var uuid: String = ""
)
