package com.strekha.quiz

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckedTextView
import android.widget.TextView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.strekha.quiz.activity.QuizActivity

class QuestionFragment() : Fragment() {

    private val questionTextView by bind<TextView>(R.id.question_text_view)
    private val firstAnswer by bind<CheckedTextView>(R.id.first_answer)
    private val secondAnswer by bind<CheckedTextView>(R.id.second_answer)
    private val thirdAnswer by bind<CheckedTextView>(R.id.third_answer)
    private val fourthAnswer by bind<CheckedTextView>(R.id.fourth_answer)
    private val banner by bind<AdView>(R.id.adView)

    private lateinit var answersTextViews: List<CheckedTextView>

    private lateinit var question: Question

    @SuppressLint("ValidFragment")
    constructor(question: Question) : this() {
        this.question = question
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_question, container, false)
    }


    override fun onResume() {
        super.onResume()

        val request = AdRequest.Builder()
                .addTestDevice("8AD778D046691727444D28428041883B")
                .build()
        banner.loadAd(request)

        questionTextView.text = Html.fromHtml(question.questionText)
        answersTextViews = listOf(firstAnswer, secondAnswer, thirdAnswer, fourthAnswer)
        val allAnswers: List<String> = question.answers()
        answersTextViews.forEachIndexed { index, textView ->
            run {
                textView.setOnClickListener {
                    if (Html.fromHtml(question.correctAnswer) == textView.text) {
                        textView.postDelayed({
                            textView.background = ColorDrawable(Color.GREEN)
                            textView.postDelayed({
                                (activity as QuizActivity).updateCount(true)
                            }, 500)
                        }, 1000)
                    } else {
                        textView.postDelayed({
                            textView.background = ColorDrawable(Color.RED)
                            answersTextViews
                                    .filter { it.text.toString() == Html.fromHtml(question.correctAnswer).toString()}
                                    .forEach { it.setTextColor(ContextCompat.getColor(context, R.color.correct)) }
                            textView.postDelayed({
                                (activity as QuizActivity).updateCount(false)
                            }, 500)
                        }, 1000)
                    }
                    question.userAnswer = textView.text.toString()
                    answersTextViews.forEach { it.isEnabled = false }
                }

                textView.text = Html.fromHtml(allAnswers[index])
            }
        }

        question.userAnswer?.let { userAnswer ->
            run {
                answersTextViews.forEach {
                    it.isEnabled = false
                    if (it.text.toString() == userAnswer) {
                        if (userAnswer == Html.fromHtml(question.correctAnswer).toString()) {
                            it.background = ColorDrawable(Color.GREEN)
                        } else {
                            it.background = ColorDrawable(Color.RED)
                            answersTextViews
                                    .filter { it.text.toString() == Html.fromHtml(question.correctAnswer).toString()}
                                    .forEach { it.setTextColor(ContextCompat.getColor(context, R.color.correct)) }
                        }
                    }
                }
            }
        }
    }
}