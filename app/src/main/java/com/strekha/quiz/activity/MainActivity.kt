package com.strekha.quiz.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.RadioGroup
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.strekha.quiz.Difficulty
import com.strekha.quiz.R
import com.strekha.quiz.activity.QuizActivity.Companion.DIFFICULTY
import com.strekha.quiz.bind
import com.strekha.quiz.launchActivity

class MainActivity : AppCompatActivity() {

    private val startButton by bind<Button>(R.id.start_button)
    private val difficulty by bind<RadioGroup>(R.id.difficulty_radio_group)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initListeners()
        supportActionBar?.title = FirebaseAuth.getInstance().currentUser?.displayName ?: "No Name"
    }

    private fun initListeners() {
        startButton.setOnClickListener {
            launchActivity<QuizActivity> {
                putExtra(DIFFICULTY, getDifficulty().name)
            }
        }
    }

    private fun getDifficulty() = when (difficulty.checkedRadioButtonId) {
        R.id.any_radio -> Difficulty.ANY
        R.id.easy_radio -> Difficulty.EASY
        R.id.medium_radio -> Difficulty.MEDIUM
        R.id.hard_radio -> Difficulty.HARD
        else -> Difficulty.ANY
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null && item.itemId == R.id.logout) {
            logout()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun logout() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener {
                    startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                    finish()
                }
    }
}
