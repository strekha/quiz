package com.strekha.quiz.activity

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    override fun onResume() {
        super.onResume()
        val auth = FirebaseAuth.getInstance()
        if (auth.currentUser != null) {
            goForward()
        } else {
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setIsSmartLockEnabled(false)
                            .setAvailableProviders(listOf(
                                    AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()
                            ))
                            .build(),
                    123
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 123 && resultCode == Activity.RESULT_OK) {
            goForward()
        } else {
            Toast.makeText(this, "Error while login.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun goForward() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}