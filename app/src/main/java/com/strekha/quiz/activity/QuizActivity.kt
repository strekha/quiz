package com.strekha.quiz.activity

import android.app.LoaderManager
import android.content.Intent
import android.content.Loader
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.strekha.quiz.*
import com.strekha.quiz.adapter.QuestionAdapter

class QuizActivity : AppCompatActivity(), LoaderManager.LoaderCallbacks<List<Question>?> {

    private val helloTextView by bind<TextView>(R.id.hello_text_view)
    private val viewPager by bind<ViewPager>(R.id.view_pager)
    private val progressBar by bind<ProgressBar>(R.id.progress_bar)
    private val difficulty by extra(DIFFICULTY)
    private val restartButton by bind<Button>(R.id.restart_button)

    private var count = 0
    private var rightCount = 0

    companion object {
        @JvmStatic
        val DIFFICULTY = "answers_num"

        @JvmStatic
        val LOADER_ID = 234
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)
        supportActionBar?.title = FirebaseAuth.getInstance().currentUser?.displayName ?: "No Name"

        restartButton.setOnClickListener{ finish() }

        loaderManager.initLoader(LOADER_ID, null, this)
    }


    override fun onLoaderReset(p0: Loader<List<Question>?>?) {

    }

    override fun onCreateLoader(p0: Int, p1: Bundle?): Loader<List<Question>?> {
        return QuestionLoader(this, difficulty) {
            onError = { Toast.makeText(this@QuizActivity, "Some error", Toast.LENGTH_LONG).show() }
            onStart = {
                log("started")
            }
        }
    }

    override fun onLoadFinished(p0: Loader<List<Question>?>?, questions: List<Question>?) {

        progressBar.visibility = View.GONE
        helloTextView.visibility = View.VISIBLE

        if (questions != null) {
            viewPager.visibility = View.VISIBLE
            viewPager.adapter = QuestionAdapter(this.supportFragmentManager, questions)
        } else {
            helloTextView.text = "Error :("
        }
    }

    override fun onDestroy() {
        if (isFinishing) {
            loaderManager.destroyLoader(LOADER_ID)
        }
        super.onDestroy()
    }

    fun updateCount(isRight: Boolean) {
        val item = viewPager.currentItem
        viewPager.currentItem = item + 1
        count++
        if (isRight) rightCount++
        if (count == 10) {
            startActivity(ResultActivity.newInstance(this@QuizActivity, rightCount))
            finish()
        }
    }


}
