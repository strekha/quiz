package com.strekha.quiz.activity

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.strekha.quiz.R
import com.strekha.quiz.Row
import com.strekha.quiz.adapter.ResultAdapter
import com.strekha.quiz.bind
import kotlin.properties.Delegates

class ResultActivity : AppCompatActivity() {

    private val resultTextView by bind<TextView>(R.id.result_text_view)
    private val resultsTable by bind<RecyclerView>(R.id.results_table)
    private val progressBar by bind<ProgressBar>(R.id.progress_bar)
    private var resultCount: Int by Delegates.notNull()

    private lateinit var adapter: ResultAdapter

    private lateinit var rootRef: DatabaseReference
    private lateinit var topRef: DatabaseReference
    private val resultArray: MutableList<Row> = mutableListOf()

    companion object {

        fun newInstance(context: Context, result: Int): Intent {
            val intent = Intent(context, ResultActivity::class.java)
            intent.putExtra("result", result)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        supportActionBar?.title = FirebaseAuth.getInstance().currentUser?.displayName ?: "No Name"
        resultCount = intent.extras?.getInt("result") ?: 0

        val result = resultCount/(10.toDouble()) * 100
        resultTextView.text = "Your result - $result%"

        adapter = ResultAdapter()
        resultsTable.layoutManager = LinearLayoutManager(this)
        resultsTable.adapter = adapter

        rootRef = FirebaseDatabase.getInstance().reference
        topRef = rootRef.child("top")

        topRef.addValueEventListener(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                resultArray.clear()
                dataSnapshot.children.mapNotNullTo(resultArray) { it.getValue<Row>(Row::class.java) }
                resultArray.sortBy { it.result }
                resultArray.reverse()
                val topResults = resultArray.take(n = 10)
                adapter.rows = topResults
                resultsTable.adapter = adapter
                resultsTable.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
        val databaseRef = topRef.push()
        val key = databaseRef.key

        val map = HashMap<String, Any>()
        map.put("person", FirebaseAuth.getInstance().currentUser?.displayName ?: "No Name")
        map.put("result", result)
        map.put("uuid", key)

        databaseRef.setValue(map)
        progressBar.visibility = View.VISIBLE
        resultsTable.visibility = View.GONE

    }
}