package com.strekha.quiz

import android.content.AsyncTaskLoader
import android.content.Context

class QuestionLoader(
        context: Context,
        private val difficulty: String,
        private val observerInit: Observer.() -> Unit = {}
) : AsyncTaskLoader<List<Question>?>(context) {

    private var questions: List<Question>? = null
    private val observer: Observer = Observer().apply { observerInit }

    override fun loadInBackground(): List<Question>? = AnswersRepository().getQuestions(difficulty) {
        observer.onError
    }

    override fun onStartLoading() {
        observer.onStart
        if (questions == null) forceLoad()
        else deliverResult(questions)
    }

    override fun deliverResult(data: List<Question>?) {
        questions = data
        super.deliverResult(data)
    }
}

class Observer {
    var onError: () -> Unit = {}
    var onStart: () -> Unit = {}

}