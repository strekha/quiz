package com.strekha.quiz.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.strekha.quiz.Question
import com.strekha.quiz.QuestionFragment

class QuestionAdapter(fragmentManager: FragmentManager, private val questions: List<Question>) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return QuestionFragment(questions[position])
    }

        override fun getCount() = questions.size
}