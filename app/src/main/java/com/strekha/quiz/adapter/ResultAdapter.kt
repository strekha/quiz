package com.strekha.quiz.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.strekha.quiz.R
import com.strekha.quiz.Row

class ResultAdapter : RecyclerView.Adapter<ResultAdapter.ResultViewHolder>() {

    var rows = emptyList<Row>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onBindViewHolder(holder: ResultViewHolder?, position: Int) {
        holder?.setContent(position, rows[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ResultViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_row, parent, false)
        return ResultViewHolder(view)
    }

    override fun getItemCount() = rows.size


    class ResultViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val numberTextView = view.findViewById<TextView>(R.id.number)
        private val nameTextView = view.findViewById<TextView>(R.id.name)
        private val percentTextView = view.findViewById<TextView>(R.id.percent)

        fun setContent(position: Int, row: Row) {
            numberTextView.text = (position + 1).toString()
            nameTextView.text = row.person
            percentTextView.text = row.result.toString()
        }
    }
}
