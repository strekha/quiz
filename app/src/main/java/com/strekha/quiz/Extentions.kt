package com.strekha.quiz

import android.app.Activity
import android.content.Intent
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import java.util.*


val random = Random()

internal inline fun <reified T : Activity> Activity.launchActivity(
        noinline init: Intent.() -> Unit = {}) {
    val intent = Intent(this, T::class.java)
    intent.init()
    startActivity(intent)
}

internal fun <T : View> Activity.bind(@IdRes id: Int): Lazy<T> =
        lazy(LazyThreadSafetyMode.NONE) { findViewById<T>(id) }

internal fun <T : View> Fragment.bind(@IdRes id: Int): Lazy<T> =
        lazy(LazyThreadSafetyMode.NONE) { view!!.findViewById<T>(id) }


internal fun Activity.extra(key: String): Lazy<String> {
    return lazy(LazyThreadSafetyMode.NONE) { intent.getStringExtra(key) }
}
internal fun <T> List<T>.shuffle(): List<T> {
    val list = this.toMutableList()
    Collections.shuffle(list)
    return list
}

fun log(any: Any) {
    Log.d("log_tag", any.toString())
}