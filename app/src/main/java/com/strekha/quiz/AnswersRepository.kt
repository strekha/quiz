package com.strekha.quiz

import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class AnswersRepository {

    fun getQuestions(difficulty: String, onError: () -> Unit = {}): List<Question>? {
        val json = sendRequest(difficulty) {
            log(it)
            onError.invoke()
        }

        json?.let {
            log(it)
            return QuestionParser.parse(it).questions
        } ?: return null
    }

    private fun sendRequest(difficulty: String, onError: (String) -> Unit = {}): String? {
        var inputStream: InputStream? = null
        var connection: HttpURLConnection? = null
        try {
            val url = URL(getUrl(difficulty))
            log(url)
            connection = url.openConnection() as HttpURLConnection
            with(connection) {
                requestMethod = "GET"
                doOutput = true
                connectTimeout = 3000
                readTimeout = 3000
                connect()
            }

            inputStream = connection.inputStream
            return inputStream.bufferedReader().use{ it.readText() }
        } catch (exception: Exception) {
            exception.message?.let { onError(it) } ?: onError("Some error occurred :(")
            return null
        }
        finally {
            inputStream?.close()
            connection?.disconnect()
        }
    }

    private fun getUrl(difficulty: String): String {

        val base = "https://opentdb.com/api.php?amount=10&category=11&type=multiple"
        return  if (difficulty == Difficulty.ANY.name) base
                else base + "&difficulty=${difficulty.toLowerCase()}"
    }

}