package com.strekha.quiz

import com.google.gson.GsonBuilder

class QuestionParser {

    companion object {

        private val gson by lazy { GsonBuilder().setPrettyPrinting().create() }

        fun parse(json: String): Result = gson.fromJson(json, Result::class.java)
    }
}